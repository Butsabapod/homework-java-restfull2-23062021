package com.example.homework2606;

import org.springframework.data.repository.CrudRepository;

public interface ComputerRepository extends CrudRepository<Computer, Integer> {

}
