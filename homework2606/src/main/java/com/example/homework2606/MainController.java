package com.example.homework2606;



import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;




@RestController
public class MainController {
	@Autowired ManagerRepository managerRepo;
	@Autowired StaffRepository staffRepo;
	@Autowired ComputerRepository computerRepo;
	@Autowired ShopRepository shopRepo;
	
	@GetMapping("/managers")
	@ResponseBody
	String getAllManagers() {
		Iterable<Manager> managers =  managerRepo.findAll();
		String out = "";
		for (Manager manager : managers) {
			out = out + manager.getName();
		}
		
		return "Get User : " + out;
	}
	
	@PostMapping(path = "/managers", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addManager(@RequestBody Manager manager ) {

		managerRepo.save(manager);
		return "Add User " + manager.getName() + " " + manager.getLastname();
	}
	
	@PutMapping(path = "/managers/{Id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateManager(@PathVariable("Id") int Id, @RequestBody Manager manager) {
		//1. Get User ID
		Manager managerFormDB = managerRepo.findById(Id).get();
		//2. Assign New Value
		managerFormDB.setName(manager.getName());
		managerFormDB.setLastname(manager.getLastname());
		managerFormDB.setSalary(manager.getSalary());
		//3. Update the new value
		managerRepo.save(managerFormDB);
		
		return "Update manager " + Id;
	}
	
	@DeleteMapping("/managers/{Id}")
	@ResponseBody
	String deleteManager(@PathVariable("Id") int Id) {
		//1. Get User ID
		Manager managerFormDB =  managerRepo.findById(Id).get();
		//2. delete value
		managerRepo.delete(managerFormDB);
		
		
		return "Delete manager " + Id;
	}
	
	//--------------------------------------------------------------------------------------
	
	@GetMapping("/staffs")
	@ResponseBody
	String getAllStaff() {
		Iterable<Staff> staffs =  staffRepo.findAll();
		String out = "";
		for (Staff staff : staffs) {
			out = out + staff.getName();
		}
		
		return "Get staff : " + out;
	}
	
	@PostMapping(path = "/staffs", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addStaff(@RequestBody Staff staff ) {

		staffRepo.save(staff);
		return "Add staff " + staff.getName() + " " + staff.getLastname();
	}
	
	@PutMapping(path = "/staffs/{Id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateStaff(@PathVariable("Id") int Id, @RequestBody Staff staff) {
		//1. Get User ID
		Staff staffFormDB = staffRepo.findById(Id).get();
		//2. Assign New Value
		staffFormDB.setName(staff.getName());
		staffFormDB.setLastname(staff.getLastname());
		staffFormDB.setSalary(staff.getSalary());
		//3. Update the new value
		staffRepo.save(staffFormDB);
		
		return "Update staff " + Id;
	}
	
	@DeleteMapping("/staffs/{Id}")
	@ResponseBody
	String deleteStaff(@PathVariable("Id") int Id) {
		//1. Get User ID
		Staff staffFormDB =  staffRepo.findById(Id).get();
		//2. delete value
		staffRepo.delete(staffFormDB);
		
		
		return "Delete staff " + Id;
	}
	
	//--------------------------------------------------------------------------------------
	
	@GetMapping("/computers")
	@ResponseBody
	String getAllComputer() {
		Iterable<Computer> computers =  computerRepo.findAll();
		String out = "";
		for (Computer computer : computers) {
			out = out + computer.getBrand();
		}
		
		return "Get computer : " + out;
	}
	
	@PostMapping(path = "/computers", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addComputer(@RequestBody Computer computer ) {

		computerRepo.save(computer);
		return "Add computer " + computer.getBrand() + " : " + computer.getCpu()+" : "+computer.getHarddisk();
	}
	
	@PutMapping(path = "/computers/{Id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateComputer(@PathVariable("Id") int Id, @RequestBody Computer computer) {
		//1. Get User ID
		Computer computerFormDB = computerRepo.findById(Id).get();
		//2. Assign New Value
		computerFormDB.setBrand(computer.getBrand());
		computerFormDB.setCpu(computer.getCpu());
		computerFormDB.setHarddisk(computer.getHarddisk());
		computerFormDB.setRam(computer.getRam());
		//3. Update the new value
		computerRepo.save(computerFormDB);
		
		return "Update computer " + Id;
	}
	
	@DeleteMapping("/computers/{Id}")
	@ResponseBody
	String deleteComputer(@PathVariable("Id") int Id) {
		//1. Get User ID
		Computer computerFormDB =  computerRepo.findById(Id).get();
		//2. delete value
		computerRepo.delete(computerFormDB);
		
		
		return "Delete computer " + Id;
	}
	
	//--------------------------------------------------------------------------------------
	
	@GetMapping("/shops")
	@ResponseBody
	String getAllShop() {
		Iterable<Shop> shops =  shopRepo.findAll();
		String out = "";
		for (Shop shop : shops) {
			out = out + shop.getName();
		}
		
		return "Get shop : " + out;
	}
	
	@PostMapping(path = "/shops", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addShop1(@RequestBody Shop shop ) {

		shopRepo.save(shop);
		return "Add shop " + shop.getName();
	}
	
	@PostMapping(path = "/addshops", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addShop(@RequestBody Shop shop ) {
		Set<Computer> computers = new HashSet<Computer>();
		Computer computer1 = new Computer();
		computer1.setBrand("AA");
		computer1.setCpu("2.7GHz");
		computer1.setHarddisk("1TB");
		computer1.setRam("2GB");
		computers.add(computer1);
		
		Computer computer2 = new Computer();
		computer2.setBrand("BB");
		computer2.setCpu("2.7GHz");
		computer2.setHarddisk("1TB");
		computer2.setRam("2GB");
		computers.add(computer2);
		
		//-------------------------------------
		
		Set<Staff> staffs = new HashSet<Staff>();
		Staff staff1 = new Staff();
		staff1.setLastname("aaa");
		staff1.setName("K.A");
		staff1.setSalary(1300);
		staffs.add(staff1);
		
		Staff staff2 = new Staff();
		staff2.setLastname("bbb");
		staff2.setName("K.B");
		staff2.setSalary(1360);
		staffs.add(staff2);
		
		shopRepo.save(shop);
		computer1.setShop(shop);
		computer2.setShop(shop);
		staff1.setShop(shop);
		staff2.setShop(shop);
		computerRepo.save(computer1);
		computerRepo.save(computer2);
		staffRepo.save(staff1);
		staffRepo.save(staff2);
		shop.setComputers(computers);
		shop.setStaffs(staffs);
		shopRepo.save(shop);
		
		return "Add shop " + shop.getName();
	}
	
	@PutMapping(path = "/shops/{Id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateShop(@PathVariable("Id") int Id, @RequestBody Shop shop) {
		//1. Get User ID
		Shop shopFormDB = shopRepo.findById(Id).get();
		//2. Assign New Value
		shopFormDB.setAddress(shop.getAddress());
		shopFormDB.setName(shop.getName());
		//3. Update the new value
		shopRepo.save(shopFormDB);
		
		return "Update shop " + Id;
	}
	
	@DeleteMapping("/shop/{Id}")
	@ResponseBody
	String deleteShop(@PathVariable("Id") int Id) {
		//1. Get User ID
		Shop shopFormDB =  shopRepo.findById(Id).get();
		//2. delete value
		shopRepo.delete(shopFormDB);
		
		
		return "Delete shop " + Id;
	}
}
